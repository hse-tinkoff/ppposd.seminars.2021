/*
Пример расчета N-го числа Фибоначчи на этапе инстанцирования шаблонов.
*/

#include <iostream>


template <int n> struct fibonacci {
    static constexpr int value = fibonacci<n - 2>::value +  fibonacci<n - 1>::value;
};

template <> struct fibonacci<0> {
    static constexpr int value = 1;
};

template <> struct fibonacci<1> {
    static constexpr int value = 1;
};


int main()
{
    std::cout << fibonacci<4>::value;
    return 0;
}

