/*
Примеры чистых функций, реализующих рекурсию (с использованием lambda-функций C++).
Первый случай - с захватом окружения, второй - без.
*/
#include <iostream>
#include <functional>

int main() {
   auto fibonacci_1 = [](int n) -> int {
        std::function<int(int,int,int)> fib = [&](int a, int b, int n) {
            return n ? fib(a + b, a, n - 1) : b;
        };
        return fib(1, 0, n);
    };

   auto fibonacci_2 = [](int n) -> int {

        auto fib = [](auto self, int a, int b, int n) -> int {
            return n ? self(self, a + b, a, n - 1) : b;
        };
        return fib(fib, 1, 0, n);
    };


    std::cout << fibonacci_2(10);
    return 0;
}

