template<typename Head, typename... Tail>
struct tuple<Head, Tail...> : tuple<Tail...>
{
    tuple(Head h, Tail... tail)
        : tuple<Tail...>(tail...), head_(h)
    {}
    typedef tuple<Tail...> base_type;
    typedef Head           value_type;

    base_type& base = static_cast<base_type&>(*this);
    Head       head_;
};

template<>
struct tuple<>
{};




tuple<int, string, vector<int>> t(14, "a", {1, 3});


t.head_
t.base.head_
t.base.base.head_
