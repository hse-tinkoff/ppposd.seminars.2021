/*
Задача: реализовать функцию, позволяющая делать объединять результаты двух функций.

combine(F f, A a, B b)(param)    |->    F(A(param), B(param))

*/


template <typename F, typename A, typename B>
auto combine(F f, A a, B b) {
    return [=] (auto ...params) {
        return f(a(params...), b(params...));
    };
}

bool or_(bool a, bool b) {
    return a || b;
}

int main()
{
    auto func = combine(or_, [](int a) {return a > 5; }, [] (int a) { return a < -5;});

    func(6);
}

