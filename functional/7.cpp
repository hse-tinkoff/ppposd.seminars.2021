/*
Задача: реализовать функцию, позволяющая рассчитывать суперпозицию функций.

r = concat(f1, f2, f3, f4)(a, b, c, d)    |->    r = f1(f2(f3(f4( a, b ))))

*/
#include<functional>

template<typename F1, typename... Fs>
auto concat(F1 f, Fs... args)
{
    if constexpr (sizeof...(args) > 0)
        return [=](auto ... params) {
            return f( concat(args...)(params...));
        };
    else // single function in list
        return [=](auto ... params) {
            return f(params...);
        };
}
int main()
{
    auto twice ( [] (int i) { return i * 2; } );
    auto thrice ( [] (int i) { return i * 3; } );

    auto combine = concat(twice, thrice, std::plus<int>{});

    auto thirty = combine(2, 3); // 30
}

