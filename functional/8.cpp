#include <iostream>
int main()
{
    auto&& function = []() {
    	int i = 0; return [=]() mutable { int arr[] = {1,2,4,8,16,16777216}; if ( i < 6 ) return arr[i++]; return 0; }; }();

    for ( unsigned long i = 0; i != 10; ++i )
        std::cout << "\t" << function() << "\t|";
    std::cout << "\n";

    return 0;
}

