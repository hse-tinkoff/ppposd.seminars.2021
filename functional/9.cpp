#include <iostream>
#include <functional>

using namespace std;

//TRUE = {x -> { y -> x}}
auto TRUE = [](auto x){
    return [x](auto y){
        return x;
    };
};

//FALSE = {x -> { y -> y}}
auto FALSE = [](auto x){
   return [](auto y){
        return y;
   };
};

//IF = {proc -> { x ->  { y -> proc(x)(y) }}}
auto IF = [](auto proc){
    return [proc](auto x){
       return [x, proc](auto y){
           return proc(x)(y);
       };
    };
};

//to_boolean = {proc -> IF(proc)(true)(false) }
auto to_boolean = [](auto proc){
    return IF(proc)(true)(false);
};

int main() {
    std::cout << IF(FALSE)("foo")("bar");
}
